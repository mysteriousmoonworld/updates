'use strict';

/* Directives */


angular.module('myApp.directives', []).
  directive('appVersion', ['version', function(version) {
    return function(scope, elm, attrs) {
      elm.text(version);
    };
  }]);

angular.module('updatesTest.directives', [])
    .directive('show-soft', function() {
        return {
            templateUrl:"./views/soft.html",
            scope:true
        }
    })
    .directive('main', function() {
        return {
            templateUrl:"./views/main.html",
            scope:true,
            transclude: true
        }
    })
    .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }])
    .directive('createsoft', function() {
    return {
        templateUrl:"./views/edit.html",
        scope:true
    }
});