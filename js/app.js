'use strict';

angular.module('updatesTest', ['updatesTest.services','admin.filters','updatesTest.directives'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
			.when('/user/:id/soft', {template: 'views/show_soft.html', controller: ListsCtrl})
            .when('/login', {template: 'views/login.html', controller: LoginCtrl})
            .when('/register', {template: 'views/register.html', controller: RegisterCtrl})
			.when('/user/:id/create', {template: 'views/edit.html', controller: CreateCtrl})
            .when('/user/:id/create/:id/update', {template: 'views/create_update.html', controller: CreateCtrl})
			//.when('/edit/:id', {template: 'views/edit.html', controller: EditCtrl})
			.otherwise({redirectTo: '/soft'});
  },
]);