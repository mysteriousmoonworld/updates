'use strict';

angular.module('updatesTest.services', ['ngResource'])
	.factory('Items', function($resource){
		return $resource('/user/'+sessionStorage.getItem('id')+'/soft', {}, {
			creation: {method:'PUT'},
			saveData: {method:'POST'},
			toggle: {method:'GET', params:{action:'toggle'}}
		});
	})
	.factory('Data', function($resource){
		var load = $resource('back/list/:name', {});
		var loadList = ['answerers','categories'];
		var data = {};
		for (var i=0; i<loadList.length; i++)
			data[loadList[i]] = load.get({name:loadList[i]});
		return function(key){ return data[key]; };
	})
    .service('fileUpload', ['$http', function ($http,$location) {
        this.post = function(uploadUrl,des,file, token){
            var fd = new FormData();
            fd.append('description', des);
            fd.append('upload', file);
            $http.post(uploadUrl, fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': 'multipart/form-data','X-Auth':token}
            })
                .then(function(response) {
                    // success
                    $location.path('/soft');
                    console.log(response);
                },
                function(response) { // optional
                    alert("Ошибка загрузки!");
                    console.log(response);
                });

        }
    }]);