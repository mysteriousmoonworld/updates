'use strict';
function ListsCtrl($scope,$location,$http) {
    $scope.description="";
    if(!sessionStorage.getItem('X-Auth')){
        $location.path('/login');
    }
    else{
        $http({
            url: 'http://localhost:8080/soft',
            method: "GET",
            headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
        })
            .then(function(response) {
                // success
                $scope.softlist=response.data;
                $scope.test=sessionStorage;
                if(sessionStorage.getItem('X-Auth')==null){
                    $rootscope.stat=true;}
                    else{
                }
            },
            function(response) { // optional
                $scope.status="failed";
                // failed
            });
    }
    $scope.getList= function(){
        $http({
            url: 'http://localhost:8080/soft',
            method: "GET",
            headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
        })
            .then(function(response) {
                // success
               $scope.test=sessionStorage.getItem('X-Auth');
            },
            function(response) { // optional
                $scope.status="failed";
                // failed
            });
    };
}
/*
function ListCtrl($scope, Items, Data) {
	$scope.Math = Math;
	$scope.items = Items.query(function(data){
		$scope.paginator.setPages($scope.items.length);
		var i = 0;
		angular.forEach(data, function(v,k) { data[k]._id = i++; });
	});
	$scope.categories = Data('categories');
	$scope.answerers  = Data('answerers');

	$scope.selected  = [];
	$scope.paginator = {
		count: 3,
		page:  1,
		pages: 1,
		setPages: function(itemsCount){ this.pages = Math.ceil(itemsCount/this.count); }
	};

	$scope.tablehead = [
		{name:'title',    title:"Заголовок",  sort:-2},
		{name:'category', title:"Категория",  sort:1, list:$scope.categories},
		{name:'answerer', title:"Кому задан", list:$scope.answerers},
		{name:'author',   title:"Автор"},
		{name:'created',  title:"Задан"},
		{name:'answered', title:"Отвечен"},
		{name:'shown',    title:"Опубликован"}
	];

	$scope.sortBy = function() {
		var order = [];
		angular.forEach($scope.tablehead, function(h){
			if (h.sort>0) order[h.sort-1] = h.name;
			if (h.sort<0) order[Math.abs(h.sort)-1] = '-'+h.name;
		});
		return order;
	};
	$scope.sortReorder = function(col,e) {
		if (e.shiftKey) {
			var sortIndex = 0;
			angular.forEach($scope.tablehead, function(el) {
				if (Math.abs(el.sort)>sortIndex) sortIndex = Math.abs(el.sort);
			});
			angular.forEach($scope.tablehead, function(el) {
				if (el.name==col) el.sort = el.sort?-el.sort:sortIndex+1;
			});
		} else {
			angular.forEach($scope.tablehead, function(el) {
				if (el.name==col) el.sort = el.sort>0?-1:1; else el.sort = null;
			});
		}
	};

	$scope.disableItem = function() {
		var item = this.item;
		Items.toggle({id:item.id}, function(data) { if (data.ok) item.shown = item.shown>0?0:1; });
	};
	$scope.deleteItem = function(one) {
		if (one) {
			var _id = $scope.selected[0];
			Items['delete']({id:$scope.items[_id].id}, function() {
				$scope.items.splice(_id,1);
				$scope.selected = [];
			});
		} else {
				var ids = [];
				angular.forEach($scope.selected, function(_id) { ids.push($scope.items[_id].id); });
				Items['delete']({ids:ids}, function(){
					angular.forEach($scope.selected, function(_id) { $scope.items.splice(_id,1); });
					$scope.selected = [];
				});
			}
	};
	$scope.selectItem = function(e) {
		if ((e.target||e.srcElement).tagName!='TD') return;
		var state = this.item.selected = !this.item.selected, _id = this.item._id;
		if (state) $scope.selected.push(_id);
			else angular.forEach($scope.selected, function(v,k) {
			       if (v==_id) { $scope.selected.splice(k,1); return false; }
			     });
	};
	$scope.$watch('items',function(newValue, oldValue) {
		if (newValue!==oldValue) $scope.paginator.setPages($scope.items.length);
	});
	$scope.$watch('paginator.page',function(page,old) {
		var newPage = page;
		if (page<1) newPage = 1;
		if (page>$scope.paginator.pages) newPage = old;
		if (typeof(newPage)=='string') newPage = +newPage.replace(/[^0-9]/,'');
		if (page!==newPage) $scope.paginator.page = newPage;
		angular.forEach($scope.items, function(v,k) { $scope.items[k].selected = false; });
	});
}

function EditCtrl($scope, $routeParams, $location, Items, Data) {
	$scope.item = Items.get({id:$routeParams.id});
	$scope.categories = Data('categories');
	$scope.answerers  = Data('answerers');
	$scope.save = function() {
		$scope.item.$save({id:$scope.item.id}, function(){ $location.path('/list'); });
	};
	// TODO wysiwyg http://deansofer.com/posts/view/14/AngularJs-Tips-and-Tricks#wysiwyg
}

function NewCtrl($scope, $location, Items, Data) {
	$scope.item = {id:0,category:'',answerer:'',title:'',text:'',answer:'',author:''};
	$scope.categories = Data('categories');
	$scope.answerers  = Data('answerers');
	$scope.save = function() {
		Items.create($scope.item, function(){ $location.path('/list'); });
	};
}
*/
function LoginCtrl($scope, $location, $http) {
    $scope.email="";
    $scope.password="";
    if(sessionStorage.getItem('X-Auth')==null){
        $scope.stat=true}
    else{
        $scope.stat=false
    }
    $scope.login = function() {
        $http({
            url: 'http://localhost:8080/login',
            method: "POST",
            data: { "email":$scope.email,"pass":$scope.password }
        })
            .then(function(response) {
                // success
                $location.path('/user/'+response.data.id+'/soft');
                sessionStorage.setItem('X-Auth',response.data.token);
                sessionStorage.setItem('role',response.data.role);
                sessionStorage.setItem('id',response.data.id);
            },
            function(response) { // optional
                $scope.status="failed";
                // failed
            });
    };
}

function RegisterCtrl($scope, $location, $http) {
    $scope.email="";
    $scope.password="";
    $scope.login = function() {
        $http({
            url: 'http://localhost:8080/register',
            method: "POST",
            data: { "email":$scope.email,"pass":$scope.password }
        })
            .then(function(response) {
                // success
                $location.path('/login');
            },
            function(response) { // optional
                $scope.status="failed";
                // failed
            });
    };
}

function LogoutCtrl($scope, $location, $http) {
                    // success
    $scope.logout = function() {
        sessionStorage.clear();
        $location.path('/login');
    }
}

function MenuCtrl($scope, $location, $http) {
    // success
    $scope.role=sessionStorage.getItem('role');
    $scope.id=sessionStorage.getItem('id');
}
function CreateCtrl($scope, $location, Items,$http,fileUpload){
    var idsoft;
    Items.creation($scope.item, function(){});
    $scope.savesoft = function() {
        $http({
            url: 'http://localhost:8080/soft',
            method: "POST",
            data: { "name":$scope.item.title,"description":$scope.item.text },
            headers : {'X-Auth':sessionStorage.getItem('X-Auth')}
        })
            .then(function(response) {
                // success
                console.log("success!");
                idsoft=response.data.id;
                $location.path('user/'+sessionStorage.getItem('id')+'/create/'+idsoft+'/update');
            },
            function(response) { // optional
                console.log("failed!");
                // failed
            });


    };
    $scope.saveupdate = function() {
        var file = $scope.myFile;
        var des =$scope.text;
        var uploadUrl = 'http://localhost:8080/soft/2/update';
       // var uploadUrl = 'http://localhost:8080/soft/'+idsoft+'/update';
        fileUpload.post(uploadUrl,des,file,sessionStorage.getItem('X-Auth'));
    };
}